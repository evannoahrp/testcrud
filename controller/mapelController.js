// const { response } = require("express");
const { Mapel } = require("../models");

module.exports = {
  index: (req, res) => {
    Mapel.findAll({})
      .then((mapel) => {
        res.json({
          data: mapel,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  create: (req, res) => {
    const { namaMapel, kriteriaKelas } = req.body;
    Mapel.create({ namaMapel, kriteriaKelas })
      .then((mapel) => {
        res.json({
          status: 200,
          message: "Berhasil",
          data: mapel,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  update: (req, res) => {
    const mapelId = req.params.id;
    const { namaMapel, kriteriaKelas } = req.body;
    Mapel.update(
      {
        namaMapel,
        kriteriaKelas,
      },
      {
        where: { id: mapelId },
      }
    )
      .then((mapel) => {
        res.json({
          status: 201,
          data: mapel,
        });
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "Gak bisa update buku",
        });
      });
  },

  delete: (req, res) => {
    const mapelId = req.params.id;
    Mapel.destroy({
      where: {
        id: mapelId,
      },
    }).then(() => {
      res.json({
        status: 200,
        message: "Buku dihapus",
      });
    });
  },
};
