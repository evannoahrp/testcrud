const router = require("express").Router();
const mapelController = require("../controller/mapelController");

router.get("/", mapelController.index);
router.post("/create", mapelController.create);
router.patch("/:id", mapelController.update);
router.delete("/:id", mapelController.delete);

module.exports = router;
