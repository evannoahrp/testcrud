const router = require("express").Router();

const mapelRouter = require("./mapelRouter");
// const cardsRouter = require("./cardRouter");
// const usersRouter = require("./userRouter");

// router.use("/", usersRouter);
router.use("/mapel", mapelRouter);
// router.use("/cards", cardsRouter);
// router.use("/users", usersRouter);

module.exports = router;
